import { registerRootComponent } from 'expo'; // Importo RegisteRootComponent para modificar la ruta de acceso a la aplicacion

import App from './src/index'; // Importo mi aplicacion a la direccion seleccionada

registerRootComponent(App);

// Al cambiar la ruta de la aplicacion TENER EN CUENTA BORRAR LA DIRECCION PREDETERMINADA DE APP.JSON
