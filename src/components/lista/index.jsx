import React, { useEffect, useState } from 'react';
import { FlatList, Text, TouchableOpacity, View } from 'react-native';
import { useSelector } from 'react-redux';

import { styles } from './styles';
import Boton from '../button';

const Lista = ({ events, onHandlerEvent, buttonON, buttonOFF, URL }) => {
  const theme = useSelector((state) => state.color.theme);
  const [deviceStates, setDeviceStates] = useState({});

  const generateRandomId = () => {
    return Math.random().toString();
  };

  const updateDeviceState = (item, state) => {
    if (state) {
      buttonON(item);
    } else {
      buttonOFF(item);
    }
    setDeviceStates((prevDeviceStates) => ({
      ...prevDeviceStates,
      [item]: state,
    }));
  };

  useEffect(() => {
    const fetchData = async () => {
      const colors = {};
      const states = {};

      for (const item of events) {
        const resp = await fetch(
          `https://proyectocoder-8130f-default-rtdb.firebaseio.com/Dispositivos/${URL}/${item}/Estado.json`
        );
        const data = await resp.json();
        colors[item] = data ? '#529446' : '#BE4949';
        states[item] = data;
      }
      setDeviceStates(states);
    };

    fetchData();
  }, [events, URL]);

  return (
    <View style={styles(theme).listContainer}>
      <FlatList
        data={events}
        renderItem={({ item }) => (
          <TouchableOpacity
            style={[
              styles(theme).itemlist,
              { backgroundColor: deviceStates[item] ? '#529446' : '#BE4949' },
            ]}
            onPress={() => onHandlerEvent(item)}>
            <Text style={styles(theme).item}>{item}</Text>
            <View style={styles(theme).displayBotones}>
              <Boton
                text="ON"
                style={styles(theme).button_input}
                styletext={styles(theme).button_text}
                onHandlerPress={() => updateDeviceState(item, true)}
              />
              <Boton
                text="OFF"
                style={styles(theme).button_input}
                styletext={styles(theme).button_text}
                onHandlerPress={() => updateDeviceState(item, false)}
              />
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={() => generateRandomId()}
      />
    </View>
  );
};

export default Lista;
