import { StyleSheet } from 'react-native';

import { getColors } from '../../constants';

export const styles = (color) => {
  const COLORS = getColors(color);

  return StyleSheet.create({
    item: {
      marginHorizontal: 20,
      fontSize: 20,
      fontFamily: 'PTSans-Bold',
      color: COLORS.text,
    },
    itemlist: {
      height: 90,
      width: 380,
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: 15,
      marginVertical: 15,
      borderColor: '#1b4332',
      borderWidth: 2,
      flexDirection: 'row',
      shadowColor: '#000',
      shadowOpacity: 0.5,
      shadowRadius: 5,
      shadowOffset: {
        width: 0,
        height: 15,
      },
    },
    listContainer: {
      flex: 1,
      backgroundColor: COLORS.primary,
      marginVertical: 20,
      alignItems: 'center',
      marginBottom: -90,
    },
    displayBotones: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    button_input: {
      width: 70,
      height: 50,
      margin: 5,
      marginRight: 15,
    },
    button_text: {
      fontSize: 16,
    },
  });
};
