import { StyleSheet } from 'react-native'; // Incluyo las propiedades de estilos de react-native

import { getColors } from '../../constants';

export const styles = (color) => {
  const COLORS = getColors(color);

  return StyleSheet.create({
    button: {
      backgroundColor: COLORS.buttons, // Modifico el color de fondo del boton
      justifyContent: 'center', // Alinea el texto en el centro horizontalmente
      alignItems: 'center', // Alinea el texto en el centro verticalmente
      width: 115, // Ajusto el largo del contenedor
      height: 50, // Ajusto el alto del contenedor
      borderRadius: 15, // Le doy un aspecto redondeado al boton
    },
    textButton: {
      color: COLORS.text,
      fontFamily: 'PTSans-BoldItalic',
      textAlign: 'center',
      fontSize: 18, // Le doy un tamaño al texto del boton
    },
  });
};
