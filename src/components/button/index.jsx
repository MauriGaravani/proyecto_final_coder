import { Text, TouchableOpacity, View } from 'react-native';
import { useSelector } from 'react-redux';

import { styles } from './styles';

const Boton = ({ text, onHandlerPress, style, styletext }) => {
  const theme = useSelector((state) => state.color.theme);

  return (
    <View style={[styles(theme).button, style]}>
      <TouchableOpacity style={[styles(theme).button, style]} onPress={() => onHandlerPress()}>
        <Text style={[styles(theme).textButton, styletext]}>{text}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Boton;
