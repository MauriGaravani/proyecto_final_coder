import { StyleSheet } from 'react-native';

import { getColors } from '../../constants';

export const styles = (color) => {
  const COLORS = getColors(color);

  return StyleSheet.create({
    modalContainer: {
      backgroundColor: COLORS.primary,
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 60,
      paddingBottom: 250,
    },
    modalTitle: {
      fontSize: 18,
      fontFamily: 'PTSans-Bold',
      marginBottom: 10,
    },
    buttonTemp: {
      paddingTop: 10,
    },
    modalDetailContainer: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    modalDetailMessage: {
      fontSize: 18,
      fontFamily: 'PTSans-Regular',
      color: COLORS.text,
      marginVertical: 20,
      textAlign: 'center',
      borderWidth: 2,
      borderColor: COLORS.item,
      borderRadius: 8,
      padding: 5,
      backgroundColor: COLORS.secondary,
    },
    selectedEvent: {
      fontSize: 18,
      color: COLORS.text,
      fontFamily: 'PTSans-BoldItalic',
      paddingVertical: 10,
      textAlign: 'center',
      marginBottom: 20,
    },
    buttonContainer: {
      width: '80%',
      justifyContent: 'space-around',
    },
    containerInput: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    inputTemp: {
      textAlign: 'center',
      borderWidth: 2,
      height: 40,
      width: 70,
      borderRadius: 5,
      borderColor: COLORS.header,
      backgroundColor: COLORS.secondary,
    },
    closeContainer: {
      marginTop: 15,
    },
  });
};
