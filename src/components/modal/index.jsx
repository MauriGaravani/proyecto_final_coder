import DateTimePicker from '@react-native-community/datetimepicker';
import React, { useEffect, useState } from 'react';
import {
  Button,
  Keyboard,
  Modal,
  Platform,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { useSelector } from 'react-redux';

import { styles } from './styles';

const ModalFondo = ({ visible, item, onDelete, onClose, onHour, OnTemp, alarma, temporizador }) => {
  const [date, setDate] = useState(new Date());
  const [system, setSystem] = useState(false);
  const [temp, setTemp] = useState('');
  const [showPicker, setShowPicker] = useState(false);
  const theme = useSelector((state) => state.color.theme);

  useEffect(() => {
    if (Platform.OS === 'android') {
      setSystem(true);
    } else {
      setSystem(false);
    }
  }, []);

  const handleNumeroChange = (text) => {
    const numericValue = text.replace(/[^0-9]/g, '');
    setTemp(numericValue);
  };

  const handleDateChange = (event, selectedDate) => {
    setShowPicker(false);

    if (selectedDate) {
      setDate(selectedDate);
      onHour(selectedDate);
    }
  };
  return (
    <Modal visible={visible} animationType="slide">
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles(theme).modalContainer}>
          <Text style={styles(theme).modalTitle}>Detalles del dispositivo</Text>
          <Text style={styles(theme).selectedEvent}>{item}</Text>
          <View style={styles(theme).modalDetailContainer}>
            <Text style={styles(theme).modalDetailMessage}>¿Deseas seleccionar una alarma?</Text>
            <View>
              {!showPicker && system && (
                <Button
                  title="Seleccionar Alarma"
                  color="#009854"
                  onPress={() => setShowPicker(true)}
                />
              )}
              {showPicker && system && (
                <DateTimePicker value={date} mode="time" is24Hour onChange={handleDateChange} />
              )}
            </View>
            {!system && (
              <DateTimePicker
                style={styles(theme).inputTemp}
                value={date}
                onChange={(event, newDate) => setDate(newDate)}
                mode="time"
              />
            )}
            {!system && (
              <Button title="Seleccionar Alarma" color="#009854" onPress={() => onHour(date)} />
            )}
            {alarma !== '' ? <Text>Alarma configurada en {alarma} minutos</Text> : null}
            <Text style={styles(theme).modalDetailMessage}>
              ¿Deseas seleccionar un temporizador?
            </Text>
            <View style={styles(theme).containerInput}>
              <TextInput
                style={styles(theme).inputTemp}
                maxLength={2}
                value={temp}
                placeholder="00"
                placeholderTextColor="#857a7a"
                onChangeText={handleNumeroChange}
                keyboardType="numeric"
              />
            </View>
            <View style={styles(theme).buttonTemp}>
              <Button
                title="Seleccionar Temporizador"
                color="#009854"
                onPress={() => OnTemp(temp)}
              />
            </View>
          </View>
          {temporizador !== '' ? (
            <Text>Temporizador configurado en {temporizador} minutos</Text>
          ) : null}
          <View style={styles(theme).buttonContainer}>
            <Text style={styles(theme).modalDetailMessage}>¿Deseas eliminar el dispositivo?</Text>
            <Button title="Eliminar Dispositivo" color="#ff0000" onPress={onDelete} />
          </View>
          <View style={styles(theme).closeContainer}>
            <Button title="Cerrar" color="#009854" onPress={onClose} />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};
export default ModalFondo;
