import {
  MediaTypeOptions,
  launchCameraAsync,
  launchImageLibraryAsync,
  requestCameraPermissionsAsync,
  requestMediaLibraryPermissionsAsync,
} from 'expo-image-picker';
import { useEffect, useState } from 'react';
import { Alert, Image, Text, View } from 'react-native';
import { useSelector } from 'react-redux';

import { styles } from './styles';
import { deleteAllPhotos, insertPhoto, selectPhoto } from '../../db';
import Boton from '../button';

export const ImageSelector = ({ onImage }) => {
  const [pickedUrl, setPickedUrl] = useState(null);
  const theme = useSelector((state) => state.color.theme);
  useEffect(() => {
    selectPhoto()
      .then((urls) => {
        setPickedUrl(urls[0]);
      })
      .catch((error) => {
        console.error('Error al obtener las URLs:', error);
      });
  });
  useEffect(() => {
    (async () => {
      const { status } = await requestMediaLibraryPermissionsAsync();
      if (status !== 'granted') {
        Alert.alert('Permiso denegado', 'Necesitamos permisos para usar la galeria de fotos', [
          { text: 'Ok' },
        ]);
      }
    })();
  }, []);

  const verifyPermissions = async () => {
    const { status } = await requestCameraPermissionsAsync();

    if (status !== 'granted') {
      Alert.alert('Permiso denegado', 'Necesitamos permisos para usar la camara', [{ text: 'Ok' }]);
      return false;
    }

    return true;
  };

  const onHandleTakeImage = async () => {
    const isCameraPermission = await verifyPermissions();
    if (!isCameraPermission) return;

    const image = await launchCameraAsync({
      allowsEditing: true,
      aspect: [16, 9],
      quality: 0.7,
    });
    setPickedUrl(image.assets[0].uri);
    onImage(image.assets[0].uri);
    deleteAllPhotos();
    insertPhoto(image.assets[0].uri);
  };

  const selectImage = async () => {
    try {
      const image = await launchImageLibraryAsync({
        mediaTypes: MediaTypeOptions.Images,
        quality: 1,
      });

      if (!image.canceled) {
        setPickedUrl(image.assets[0].uri);
        deleteAllPhotos();
        insertPhoto(image.assets[0].uri);
      }
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  return (
    <View>
      <View style={styles(theme).preview}>
        {!pickedUrl ? (
          <Text>No hay imagen seleccionada</Text>
        ) : (
          <Image style={styles(theme).image} source={{ uri: pickedUrl }} />
        )}
      </View>
      <View style={styles(theme).botonesContainer}>
        <Boton text="Tomar Foto" style={styles(theme).Boton} onHandlerPress={onHandleTakeImage} />
        <Boton text="Seleccionar Imagen" style={styles(theme).Boton} onHandlerPress={selectImage} />
      </View>
    </View>
  );
};

export default ImageSelector;
