import { StyleSheet } from 'react-native';

import { getColors } from '../../constants';

export const styles = (color) => {
  const COLORS = getColors(color);

  return StyleSheet.create({
    preview: {
      width: 350,
      height: 350,
      marginBottom: 20,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 15,
      borderWidth: 10,
      borderColor: COLORS.buttons,
    },
    image: {
      width: '100%',
      height: '100%',
      borderRadius: 5,
    },
    Boton: {
      marginHorizontal: 30,
    },
    botonesContainer: {
      flexDirection: 'row',
    },
  });
};
