import Ionicons from '@expo/vector-icons/Ionicons';
import { useState } from 'react';
import { SafeAreaView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { useSelector } from 'react-redux';

import { styles } from './styles';

const Auth_Input = ({
  value,
  onChangeText,
  placeholder,
  hidePassword,
  label,
  mandatory,
  ...props
}) => {
  const [pass, setPass] = useState(hidePassword);
  const theme = useSelector((state) => state.color.theme);

  return (
    <SafeAreaView>
      <View style={styles(theme).inputsContainer}>
        <View style={styles(theme).errorContainer}>
          <Text style={styles(theme).label}>{label}</Text>
          {mandatory && <Text style={styles(theme).errorMessage}> * CAMPO OBLIGATORIO *</Text>}
        </View>
        <View style={styles(theme).passwordContainer}>
          <TextInput
            value={value}
            style={{ ...styles(theme).input }}
            onChangeText={onChangeText}
            secureTextEntry={pass}
            placeholder={placeholder}
            placeholderTextColor="#7F7F7F"
          />
          {hidePassword && (
            <TouchableOpacity style={styles(theme).icon} onPress={() => setPass(!pass)}>
              <Ionicons name={pass ? 'eye' : 'eye-off'} size={26} />
            </TouchableOpacity>
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Auth_Input;
