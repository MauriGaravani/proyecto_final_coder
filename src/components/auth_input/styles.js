import { StyleSheet } from 'react-native';

import { getColors } from '../../constants';

export const styles = (color) => {
  const COLORS = getColors(color);

  return StyleSheet.create({
    input: {
      fontSize: 20,
      borderColor: COLORS.secondary,
      borderBottomWidth: 2.5,
      width: '100%',
      height: 35,
      fontFamily: 'PTSans-Italic',
      color: COLORS.black,
      textAlign: 'left',
    },
    errorMessage: {
      fontSize: 13,
      fontFamily: 'PTSans-Regular',
      paddingVertical: 2,
      color: COLORS.brightRed,
    },
    label: {
      marginBottom: 12,
      fontSize: 16,
      color: COLORS.text,
      fontFamily: 'PTSans-Regular',
    },
    inputsContainer: {
      paddingVertical: 20,
    },
    passwordContainer: {
      flexDirection: 'row',
      alignItems: 'flex-end',
    },
    icon: {
      marginLeft: -25,
      paddingVertical: 5,
    },
    errorContainer: {
      flexDirection: 'row',
    },
  });
};
