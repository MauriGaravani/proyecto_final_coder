import { StyleSheet } from 'react-native';

import { getColors } from '../../constants';

export const styles = (color) => {
  const COLORS = getColors(color);

  return StyleSheet.create({
    inputContainer: {
      flexDirection: 'row',
      marginTop: -30,
      justifyContent: 'center',
    },
    input: {
      width: '70%',
      fontSize: 20,
      color: COLORS.text,
      marginRight: 15,
      borderColor: 'black',
      borderWidth: 2,
      borderRadius: 15,
      backgroundColor: COLORS.secondary,
      padding: 10,
      fontFamily: 'PTSans-Regular',
      shadowOpacity: 0.5,
      shadowRadius: 5,
      shadowOffset: {
        width: 8,
        height: 5,
      },
    },
    button_input: {
      backgroundColor: COLORS.buttons,
      justifyContent: 'center',
      alignItems: 'center',
      width: 80,
      height: 50,
      borderRadius: 8,
      shadowOpacity: 0.5,
      shadowRadius: 5,
      shadowOffset: {
        width: 8,
        height: 5,
      },
    },
  });
};
