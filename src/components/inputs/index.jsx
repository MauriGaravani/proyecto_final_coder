import React from 'react';
import { TextInput, View } from 'react-native';
import { useSelector } from 'react-redux';

import { styles } from './styles';
import Boton from '../button';

const Input = ({ placeholder, value, onChangeText, buttonTitle, onHandlerButton }) => {
  const theme = useSelector((state) => state.color.theme);

  return (
    <View style={styles(theme).inputContainer}>
      <TextInput
        style={styles(theme).input}
        placeholder={placeholder}
        value={value}
        onChangeText={onChangeText}
        placeholderTextColor="gray"
        maxLength={14}
        onSubmitEditing={() => {
          onHandlerButton();
        }}
      />
      <Boton
        text={buttonTitle}
        style={styles(theme).button_input}
        onHandlerPress={onHandlerButton}
      />
    </View>
  );
};
export default Input;
