import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('photos.db');
const db2 = SQLite.openDatabase('auth.db');

export const init = () => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS photos (image TEXT NOT NULL)',
        [],
        () => {
          resolve();
        },
        (_, err) => {
          reject(err);
        }
      );
    });
    db2.transaction((tx) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS auth (id INTEGER PRIMARY KEY AUTOINCREMENT, url TEXT NOT NULL, active INTEGER)',
        [],
        () => {
          resolve();
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });

  return promise;
};

export const insertPhoto = (image) => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        'INSERT INTO photos (image) VALUES (?)',
        [image],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });

  return promise;
};

export const selectPhoto = () => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT image FROM photos',
        [],
        (_, result) => {
          const rows = result.rows;
          const photos = [];

          for (let i = 0; i < rows.length; i++) {
            const { image } = rows.item(i);
            photos.push(image);
          }

          resolve(photos);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });

  return promise;
};

export const deleteAllPhotos = () => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((tx) => {
      tx.executeSql(
        'DELETE FROM photos',
        [],
        (_, result) => {
          resolve(result);
        },
        (_, error) => {
          reject(error);
        }
      );
    });
  });

  return promise;
};

export const authID = (id, active) => {
  const promise = new Promise((resolve, reject) => {
    db2.transaction((tx) => {
      tx.executeSql(
        'INSERT INTO auth (url, active) VALUES (?, ?)',
        [id, active],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });

  return promise;
};

export const selectAuth = () => {
  const promise = new Promise((resolve, reject) => {
    db2.transaction((tx) => {
      tx.executeSql(
        'SELECT url, active FROM auth',
        [],
        (_, result) => {
          resolve(result);
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
  return promise;
};
export const deleteAuth = () => {
  const promise = new Promise((resolve, reject) => {
    db2.transaction((tx) => {
      tx.executeSql(
        'DELETE FROM auth',
        [],
        (_, result) => {
          resolve(result);
        },
        (_, error) => {
          reject(error);
        }
      );
    });
  });
  return promise;
};
