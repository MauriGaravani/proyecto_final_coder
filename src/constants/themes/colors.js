export const getColors = (theme) => {
  if (theme === 'light') {
    return {
      primary: '#EAF4F9',
      secondary: '#CCE3DE',
      buttons: '#A4C3B2',
      text: '#212121',
      item: '#6B9080',
      header: '#6B9059',
      headerText: '#212121',
      black: '#000000',
      white: '#FFFFFF',
      lightGray: '#F5F5F5',
      darkGray: '#7F7F7F',
      brightRed: '#E23428',
    };
  } else if (theme === 'dark') {
    return {
      primary: '#4C5C68',
      secondary: '#C5C3C6',
      buttons: '#46494C',
      text: '#FFFFFF',
      item: '#8E8E93',
      header: '#8E8E93',
      headerText: '#FFFFFF',
      black: '#000000',
      white: '#FFFFFF',
      lightGray: '#F5F5F5',
      darkGray: '#7F7F7F',
      brightRed: '#E23428',
    };
  }
};
