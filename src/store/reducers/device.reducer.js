import { devicesTypes } from '../types';

const { ADD_DEVICE, REMOVE_DEVICE, REMOVE_ALL_DEVICE } = devicesTypes;

const initialState = {
  devices: [],
};

const deviceReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DEVICE:
      return {
        devices: [...state.devices, action.device],
      };
    case REMOVE_DEVICE:
      try {
        fetch(
          'https://proyectocoder-8130f-default-rtdb.firebaseio.com/Dispositivos/' +
            action.Url +
            '/' +
            action.device +
            '.json',
          {
            method: 'DELETE',
          }
        );
      } catch (error) {
        console.log(error);
      }
      return {
        devices: state.devices.filter((device) => device !== action.device),
      };
    case REMOVE_ALL_DEVICE:
      return {
        devices: [],
      };
    default:
      return state;
  }
};

export default deviceReducer;
