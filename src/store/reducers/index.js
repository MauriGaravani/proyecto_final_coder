export { default as deviceReducer } from './device.reducer';
export { default as authReducer } from './auth.reducer';
export { default as datosReducer } from './datos.reducer';
export { default as colorReducer } from './color.reducer';
