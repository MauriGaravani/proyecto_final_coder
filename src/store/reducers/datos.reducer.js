const initialState = {
  datos: null,
  UTC: null,
};

const datosReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ACTUALIZAR_DATOS':
      return {
        ...state,
        datos: action.payload,
      };
    case 'VISUALIZAR_DATOS':
      return state.datos;

    case 'ACTUALIZAR_UTC':
      try {
        fetch(
          'https://proyectocoder-8130f-default-rtdb.firebaseio.com/Usuarios/' +
            action.URL +
            '/.json',
          {
            method: 'PATCH',
            body: JSON.stringify({
              UTC: parseInt(action.payload, 10),
            }),
          }
        );
      } catch (error) {
        console.log(error);
      }
      return {
        ...state,
        UTC: action.payload,
      };

    default:
      return state;
  }
};

export default datosReducer;
