import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

import { authReducer, colorReducer, datosReducer, deviceReducer } from './reducers/index';

const rootReducer = combineReducers({
  device: deviceReducer,
  auth: authReducer,
  datos: datosReducer,
  color: colorReducer,
});

export default createStore(rootReducer, applyMiddleware(thunk));
