import { FIREBASE_AUTH_SIGN_IN_URL, FIREBASE_AUTH_SIGN_UP_URL } from '../../constants';
import { authTypes } from '../types';

const { SIGN_UP } = authTypes;

export const signUp = ({ email, password, nombre, utc }) => {
  return async (dispatch) => {
    try {
      const response = await fetch(FIREBASE_AUTH_SIGN_UP_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email,
          password,
          returnSecureToken: true,
        }),
      });
      const data = await response.json();
      if (data.email === email.toLowerCase()) {
        try {
          fetch(
            'https://proyectocoder-8130f-default-rtdb.firebaseio.com/Usuarios/' +
              data.localId +
              '.json',
            {
              method: 'PATCH',
              body: JSON.stringify({
                Email: email,
                Nombre: nombre,
                UTC: parseInt(utc, 10),
              }),
            }
          );
        } catch (error) {
          console.log(error);
        }
      }
      dispatch({
        type: SIGN_UP,
        token: data.idToken,
        userId: data.localId,
      });
      return data;
    } catch (error) {
      console.log('el error es', error.message);
      throw error.message;
    }
  };
};

export const signIn = ({ email, password }) => {
  return async (dispatch) => {
    try {
      const response = await fetch(FIREBASE_AUTH_SIGN_IN_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email,
          password,
          returnSecureToken: true,
        }),
      });

      const data = await response.json();
      return data;
    } catch (error) {
      throw error;
    }
  };
};
