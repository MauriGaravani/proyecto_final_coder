export const actualizarDatos = (datos) => ({
  type: 'ACTUALIZAR_DATOS',
  payload: datos,
});
export const verDatos = (datos) => ({
  type: 'VISUALIZAR_DATOS',
  payload: datos,
});
export const actualizarUTC = (datos, url) => ({
  type: 'ACTUALIZAR_UTC',
  payload: datos,
  URL: url,
});
