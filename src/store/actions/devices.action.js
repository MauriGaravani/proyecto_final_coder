import { devicesTypes } from '../types';

const { ADD_DEVICE, REMOVE_DEVICE, REMOVE_ALL_DEVICE } = devicesTypes;

export const addDevice = (id) => ({
  type: ADD_DEVICE,
  device: id,
});

export const removeDevice = (id, url) => ({
  type: REMOVE_DEVICE,
  device: id,
  Url: url,
});
export const removeAllDevice = (id) => ({
  type: REMOVE_ALL_DEVICE,
  device: id,
});
