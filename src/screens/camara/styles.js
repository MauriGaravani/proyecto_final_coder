import { StyleSheet } from 'react-native';

import { getColors } from '../../constants';

export const styles = (color) => {
  const COLORS = getColors(color);

  return StyleSheet.create({
    container: {
      alignItems: 'center',
      backgroundColor: COLORS.primary,
    },
    tinyLogo: {
      width: 350,
      height: 350,
      borderRadius: 15,
      borderWidth: 10,
      borderColor: COLORS.buttons,
      marginTop: 40,
    },
    captura: {
      flexDirection: 'row',
      marginTop: -130,
    },
    boton: {
      marginVertical: 140,
      marginRight: 30,
      marginLeft: 30,
    },
    preview: {
      width: 350,
      height: 350,
      marginBottom: 20,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 15,
      borderWidth: 10,
      borderColor: COLORS.buttons,
      marginVertical: 25,
    },
    image: {
      width: '100%',
      height: '100%',
    },
    text: {
      textAlign: 'center',
    },
    Contitle: {
      alignItems: 'center',
      marginVertical: 20,
    },
    title: {
      fontFamily: 'PTSans-BoldItalic',
      fontSize: 18,
    },
  });
};
