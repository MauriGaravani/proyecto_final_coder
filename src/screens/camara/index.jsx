import { useEffect, useState } from 'react';
import { Image, SafeAreaView, ScrollView, Text, View, Alert } from 'react-native';
import { useSelector } from 'react-redux';

import { styles } from './styles';
import { Boton, ImageSelector } from '../../components';
import { selectAuth } from '../../db';

const Camara = () => {
  const theme = useSelector((state) => state.color.theme);
  const [imageError, setImageError] = useState(false);
  const [url, setUrl] = useState('');
  useEffect(() => {
    selectAuth()
      .then((result) => {
        setUrl(result.rows._array[0].url);
      })
      .catch((error) => {
        console.error('Error al obtener las URLs:', error);
      });
  }, []);

  const uploadPhoto = (flash) => {
    if (flash) Alert.alert('Foto Flash en proceso... Falta terminar conexion de ESP32-CAM');
    else Alert.alert('Foto sin flash en proceso... Falta terminar conexion de ESP32-CAM');
    try {
      fetch(
        'https://proyectocoder-8130f-default-rtdb.firebaseio.com/Usuarios/' + url + '/Foto.json',
        {
          method: 'PATCH',
          body: JSON.stringify({
            State: true,
            Flash: flash,
          }),
        }
      );
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <SafeAreaView style={styles(theme).container}>
      <ScrollView>
        <View style={styles(theme).Contitle}>
          <Text style={styles(theme).title}>IMAGEN PRE-CARGADA</Text>
        </View>
        <View style={styles(theme).container}>
          <ImageSelector onImage={() => null} />
          <View style={styles(theme).Contitle}>
            <Text style={styles(theme).title}>IMAGEN TIEMPO REAL</Text>
          </View>
          <View style={styles(theme).preview}>
            {imageError ? (
              <Text style={styles(theme).text}>
                No hay imagen seleccionada, captura una nueva con o sin flash
              </Text>
            ) : (
              <Image
                style={styles(theme).image}
                onError={() => setImageError(true)}
                onLoad={() => setImageError(false)}
                source={{
                  uri:
                    'https://firebasestorage.googleapis.com/v0/b/fotos-impresora.appspot.com/o/data%2F' +
                    url +
                    '.jpg?alt=media',
                }}
              />
            )}
          </View>
          <View style={styles(theme).captura}>
            <Boton
              text="Flash"
              style={styles(theme).boton}
              onHandlerPress={() => uploadPhoto(true)}
            />
            <Boton
              text="Sin Flash"
              style={styles(theme).boton}
              onHandlerPress={() => uploadPhoto(false)}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Camara;
