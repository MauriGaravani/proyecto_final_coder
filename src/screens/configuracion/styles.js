import { StyleSheet } from 'react-native';

import { getColors } from '../../constants';

export const styles = (color) => {
  const COLORS = getColors(color);

  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: COLORS.primary,
    },
    containerAll: {
      marginVertical: 20,
    },
    containerSwitch: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 20,
    },
    switch: {
      marginLeft: 20,
      paddingBottom: 10,
    },
    textSwitch: {
      fontFamily: 'PTSans-Italic',
      fontSize: 17,
    },
    textToggle: {
      paddingLeft: 6,
      paddingTop: 1.5,
    },
    ContConf: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    textConf: {
      fontFamily: 'PTSans-BoldItalic',
      fontSize: 30,
      marginRight: 10,
    },
    linkText: {
      color: COLORS.brightRed,
    },
    linkContainer: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    link: {
      flexDirection: 'row',
      justifyContent: 'center',
    },
    inputTemp: {
      textAlign: 'center',
      borderWidth: 2,
      height: 40,
      width: 70,
      borderRadius: 5,
      backgroundColor: COLORS.secondary,
    },
    containerHour: {
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 10,
    },
    containerInput: {
      alignSelf: 'center',
      margin: 10,
    },
    separator: {
      borderBottomColor: 'black',
      borderBottomWidth: 1,
      width: '70%',
      marginVertical: 10,
      alignSelf: 'center',
    },
  });
};
