import { useEffect, useState } from 'react';
import {
  SafeAreaView,
  Switch,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { styles } from './styles';
import { authID, deleteAllPhotos, deleteAuth, selectAuth } from '../../db';
import { actualizarDatos, removeAllDevice, setearColor, actualizarUTC } from '../../store/actions';

const Acceso = () => {
  const UTC = useSelector((state) => state.datos.UTC);
  const theme = useSelector((state) => state.color.theme);
  const dispatch = useDispatch();
  const [isDarkMode, setIsDarkMode] = useState(false);
  const [zoneHour, setZoneHour] = useState('');

  useEffect(() => {
    async function handleNumeroChange() {
      const result = await selectAuth();
      const urlValue = result.rows._array[0].url;
      fetch(
        'https://proyectocoder-8130f-default-rtdb.firebaseio.com/Usuarios/' + urlValue + '/.json'
      )
        .then((response) => response.json())
        .then((data) => {
          dispatch(actualizarUTC(data.UTC, urlValue));
        })
        .catch((error) => {
          console.log('Error fetching data:', error);
        });
    }
    handleNumeroChange();
  }, []);
  const toggleColorMode = () => {
    setIsDarkMode((previousState) => !previousState);
    dispatch(isDarkMode ? setearColor('light') : setearColor('dark'));
  };
  const onChangeLogout = () => {
    dispatch(actualizarDatos(false));
    deleteAuth();
    deleteAllPhotos();
    dispatch(removeAllDevice());
    authID('', false);
  };
  async function handleNumeroChange() {
    const result = await selectAuth();
    const urlValue = result.rows._array[0].url;
    if (zoneHour !== '') dispatch(actualizarUTC(zoneHour, urlValue));
    setZoneHour();
  }
  const handleInputChange = (text) => {
    const cleanedValue = text.replace(/[^0-9-]/g, '');

    if (cleanedValue === '') {
      setZoneHour('');
    } else {
      const numericValue = parseInt(cleanedValue, 10);
      if (
        (!isNaN(numericValue) && numericValue >= -11 && numericValue <= 12) ||
        cleanedValue === '-'
      ) {
        setZoneHour(cleanedValue);
      }
    }
  };
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles(theme).container}>
        <View style={styles(theme).containerAll}>
          <View style={styles(theme).ContConf}>
            <Text style={styles(theme).textConf}>Configuración</Text>
          </View>
          <View style={styles(theme).separator} />
          <View style={styles(theme).containerSwitch}>
            <Text style={styles(theme).textSwitch}>Tema de la aplicación: </Text>
            <Text style={styles(theme).textToggle}>
              {!isDarkMode ? 'Modo Claro' : 'Modo Oscuro'}
            </Text>
            <Switch
              style={styles(theme).switch}
              trackColor={{ false: '#767577', true: '#7F7F7F' }}
              thumbColor={isDarkMode ? '#CCE3DE' : '#f4f3f4'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleColorMode}
              value={isDarkMode}
            />
          </View>
          <View style={styles(theme).separator} />
          <View style={styles(theme).containerHour}>
            <Text style={styles(theme).textSwitch}>Zona Horaria ({UTC})</Text>
            <View style={styles(theme).containerInput}>
              <TextInput
                style={styles(theme).inputTemp}
                value={zoneHour}
                placeholder="00"
                placeholderTextColor="#857a7a"
                onChangeText={handleInputChange}
              />
            </View>
          </View>
          <TouchableOpacity style={styles(theme).link} onPress={() => handleNumeroChange()}>
            <Text style={styles(theme).linkHour}>Aplicar</Text>
          </TouchableOpacity>
          <View style={styles(theme).separator} />
          <TouchableOpacity style={styles(theme).link} onPress={() => onChangeLogout()}>
            <Text style={styles(theme).linkText}>Cerrar Sesion</Text>
          </TouchableOpacity>
          <View style={styles(theme).separator} />
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

export default Acceso;
