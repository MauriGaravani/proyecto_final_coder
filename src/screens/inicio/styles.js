import { StyleSheet } from 'react-native';

import { getColors } from '../../constants';

export const styles = (color) => {
  const COLORS = getColors(color);

  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: COLORS.primary,
      paddingVertical: 40,
    },
    header: {
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      marginVertical: 60,
    },
    ContentName: {
      marginLeft: 10,
      alignContent: 'center',
      alignItems: 'flex-start',
      backgroundColor: COLORS.secondary,
      borderWidth: 2,
      borderColor: COLORS.item,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 40,
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 40,
      padding: 10,
      width: '40%',
    },
    name: {
      fontFamily: 'PTSans-Italic',
      fontSize: 18,
    },
  });
};
