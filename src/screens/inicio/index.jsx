import moment from 'moment';
import { useEffect, useState } from 'react'; // Importo el hook useState
import {
  Keyboard,
  Platform,
  SafeAreaView,
  StatusBar,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { styles } from './styles';
import { Input, Lista, ModalFondo } from '../../components';
import { selectAuth } from '../../db';
import { actualizarUTC, addDevice, removeDevice } from '../../store/actions';

const Inicio = ({ navigation }) => {
  const devices = useSelector((state) => state.device.devices);
  const theme = useSelector((state) => state.color.theme);
  const UTC = useSelector((state) => state.datos.UTC);
  const dispatch = useDispatch();

  const [modalVisible, setModalVisible] = useState(false);
  const [selectedEvent, setSelectedEvent] = useState(null);
  const [text, setText] = useState('');
  const [url, setUrl] = useState('');
  const [name, setName] = useState('');
  const [alarma, setAlarma] = useState('');
  const [temporizador, setTemporizador] = useState('');
  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await selectAuth();
        if (result.rows._array && result.rows._array.length > 0) {
          const urlValue = result.rows._array[0].url;
          setUrl(urlValue);
          const resp = await fetch(
            `https://proyectocoder-8130f-default-rtdb.firebaseio.com/Usuarios/${urlValue}/.json`
          );
          const datas = await resp.json();
          setName(datas.Nombre);
          dispatch(actualizarUTC(datas.UTC, urlValue));
          const response = await fetch(
            `https://proyectocoder-8130f-default-rtdb.firebaseio.com/Dispositivos/${urlValue}/.json`
          );
          let data = await response.json();
          if (data === null) {
            data = {};
          }
          const claves = Object.keys(data);
          claves.map((texto) => {
            dispatch(addDevice(texto));
          });
        } else {
          console.error('No se encontraron datos en result.rows._array');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchData();
  }, []);

  function onAddEvent(text) {
    if (text.length === 0) return;
    dispatch(addDevice(text));
    try {
      fetch(
        `https://proyectocoder-8130f-default-rtdb.firebaseio.com/Dispositivos/${url}/${text}.json`,
        {
          method: 'PATCH',
          body: JSON.stringify({
            Estado: false,
          }),
        }
      );
    } catch (error) {
      console.log(error);
    }
    setText('');
  }

  function onHandlerEvent(id) {
    setModalVisible(!modalVisible);
    setSelectedEvent(id);
  }
  function CloseModal() {
    setModalVisible(!modalVisible);
    setModalVisible(null);
  }

  function DeleteItem(id) {
    dispatch(removeDevice(id, url));
    setModalVisible(!modalVisible);
  }
  async function onTemp(temp) {
    if (temp !== '') {
      try {
        await fetch(
          `https://proyectocoder-8130f-default-rtdb.firebaseio.com/Dispositivos/${url}/${selectedEvent}.json`,
          {
            method: 'PATCH',
            body: JSON.stringify({
              Alarma: parseInt(temp, 10),
              State: true,
            }),
          }
        );
        setTemporizador(parseInt(temp, 10));
        setAlarma('');
      } catch (error) {
        console.log(error);
      }
    }
  }

  function convertToGMTPlus3(time) {
    const [hour, minute] = time.split(':');
    const date = new Date();
    date.setUTCHours(parseInt(hour, 10) + parseInt(UTC, 10));
    date.setUTCMinutes(parseInt(minute, 10));

    const formattedTime = `${('0' + date.getUTCHours()).slice(-2)}:${(
      '0' + date.getUTCMinutes()
    ).slice(-2)}`;
    return formattedTime;
  }

  function calculateTimeUntilAlarm(alarmTime, hournow) {
    if (Platform.OS !== 'android') {
      const currentTime = new Date();
      const currentHour = currentTime.getHours();
      const currentMinute = currentTime.getMinutes();
      const [alarmHour, alarmMinute] = alarmTime.split(':');

      const totalCurrentMinutes = currentHour * 60 + currentMinute;
      const totalAlarmMinutes = parseInt(alarmHour, 10) * 60 + parseInt(alarmMinute, 10);

      let remainingMinutes = 0;

      if (totalCurrentMinutes <= totalAlarmMinutes) {
        remainingMinutes = totalAlarmMinutes - totalCurrentMinutes;
      } else {
        remainingMinutes = 24 * 60 - totalCurrentMinutes + totalAlarmMinutes;
      }

      const hours = Math.floor(remainingMinutes / 60);
      const minutes = remainingMinutes % 60;

      return hours * 60 + minutes;
    } else {
      const [currentHour, currentMinute] = hournow.split(':');
      const [alarmHour, alarmMinute] = alarmTime.split(':');

      const totalCurrentMinutes = parseInt(currentHour, 10) * 60 + parseInt(currentMinute, 10);
      const totalAlarmMinutes = parseInt(alarmHour, 10) * 60 + parseInt(alarmMinute, 10);
      let remainingMinutes = 0;

      if (totalCurrentMinutes <= totalAlarmMinutes) {
        remainingMinutes = totalAlarmMinutes - totalCurrentMinutes;
      } else {
        remainingMinutes = 24 * 60 - totalCurrentMinutes + totalAlarmMinutes;
      }

      const hours = Math.floor(remainingMinutes / 60);
      const minutes = remainingMinutes % 60;

      return hours * 60 + minutes;
    }
  }
  async function onHour(hour) {
    if (Platform.OS === 'android') {
      const hournow = moment();
      const hours = convertToGMTPlus3(
        parseInt(JSON.stringify(hournow).slice(12, 14), 10) +
          ':' +
          parseInt(JSON.stringify(hournow).slice(15, 17), 10)
      );
      const diferencia = calculateTimeUntilAlarm(
        parseInt(JSON.stringify(hour).slice(12, 14), 10) +
          ':' +
          parseInt(JSON.stringify(hour).slice(15, 17), 10),
        hours
      );
      try {
        await fetch(
          `https://proyectocoder-8130f-default-rtdb.firebaseio.com/Dispositivos/${url}/${selectedEvent}.json`,
          {
            method: 'PATCH',
            body: JSON.stringify({
              Alarma: diferencia,
              State: true,
            }),
          }
        );
        setAlarma(diferencia);
        setTemporizador('');
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        const selectedHour = convertToGMTPlus3(
          parseInt(JSON.stringify(hour).slice(12, 14), 10) +
            ':' +
            parseInt(JSON.stringify(hour).slice(15, 17), 10)
        );
        const diferencia = calculateTimeUntilAlarm(selectedHour);
        await fetch(
          `https://proyectocoder-8130f-default-rtdb.firebaseio.com/Dispositivos/${url}/${selectedEvent}.json`,
          {
            method: 'PATCH',
            body: JSON.stringify({
              Alarma: diferencia,
              State: true,
            }),
          }
        );
        setAlarma(diferencia);
        setTemporizador('');
      } catch (error) {
        console.log(error);
      }
    }
  }
  async function buttonON(id) {
    try {
      await fetch(
        `https://proyectocoder-8130f-default-rtdb.firebaseio.com/Dispositivos/${url}/${id}.json`,
        {
          method: 'PATCH',
          body: JSON.stringify({
            Estado: true,
          }),
        }
      );
    } catch (error) {
      console.log(error);
    }
  }
  function buttonOFF(id) {
    try {
      fetch(
        `https://proyectocoder-8130f-default-rtdb.firebaseio.com/Dispositivos/${url}/${id}.json`,
        {
          method: 'PATCH',
          body: JSON.stringify({
            Estado: false,
          }),
        }
      );
    } catch (error) {
      console.log(error);
    }
  }
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles(theme).container}>
        <View style={styles(theme).ContentName}>
          <Text style={styles(theme).name}>Hola, {name}!</Text>
        </View>
        <StatusBar barStyle="dark-content" />
        <View style={styles(theme).container}>
          <Input
            placeholder="Ingrese un nuevo dispositivo"
            buttonTitle="Agregar"
            value={text}
            onChangeText={(text) => setText(text)}
            onHandlerButton={() => onAddEvent(text)}
            style={styles(theme).input}
          />
          <Lista
            events={devices}
            onHandlerEvent={onHandlerEvent}
            buttonON={buttonON}
            buttonOFF={buttonOFF}
            URL={url}
          />
          <ModalFondo
            visible={modalVisible}
            item={selectedEvent}
            onClose={CloseModal}
            onDelete={() => DeleteItem(selectedEvent)}
            onHour={onHour}
            OnTemp={onTemp}
            alarma={alarma}
            temporizador={temporizador}
          />
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

export default Inicio;
