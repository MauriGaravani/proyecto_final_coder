import { StyleSheet } from 'react-native';

import { getColors } from '../../constants';

export const styles = (color) => {
  const COLORS = getColors(color);

  return StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      marginVertical: 30,
    },
    inputs: {
      width: '85%',
      maxWidth: 400,
      padding: 15,
      margin: 15,
      backgroundColor: COLORS.white,
      borderWidth: 1,
      borderColor: COLORS.primary,
      shadowColor: '#000',
      shadowOffset: {
        width: 3,
        height: 2,
      },
      shadowOpacity: 0.32,
      shadowRadius: 2.22,
      elevation: 3,
    },
    title: {
      fontFamily: 'PTSans-Italic',
      textAlign: 'center',
      marginBottom: 20,
      fontSize: 25,
      color: COLORS.black,
    },

    linkContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 10,
    },
    link: {
      borderBottomColor: COLORS.primary,
      borderBottomWidth: 1,
      textAlign: 'center',
    },
    linkText: {
      fontFamily: 'PTSans-Regular',
      fontSize: 14,
      borderBottomColor: '#0582CA',
      borderBottomWidth: 1,
      textAlign: 'center',
      color: '#009854',
    },
    submitContainer: {
      paddingVertical: 5,
    },
    errorMessage: {
      fontSize: 14,
      fontFamily: 'PTSans-Regular',
      paddingVertical: 5,
      alignItems: 'center',
      justifyContent: 'center',
      color: COLORS.brightRed,
    },
    errorContainer: {
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
};
