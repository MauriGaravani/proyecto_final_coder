import { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Button,
  Keyboard,
  SafeAreaView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { CheckBox } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';

import { styles } from './styles';
import { Auth_Input } from '../../components';
import { authID, deleteAuth, selectAuth } from '../../db';
import { actualizarDatos, signIn, signUp } from '../../store/actions';

const Acceso = () => {
  const theme = useSelector((state) => state.color.theme);
  const dispatch = useDispatch();
  const [isLogin, setIsLogin] = useState(true);
  const [check, setCheck] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [mail, setMail] = useState('');
  const [pass, setPass] = useState('');
  const [name, setName] = useState('');
  const [zoneHour, setZoneHour] = useState('');
  const [error, setError] = useState('');

  const [mandatoryM, setMandatoryM] = useState(false);
  const [mandatoryP, setMandatoryP] = useState(false);
  const [mandatoryN, setMandatoryN] = useState(false);
  const [mandatoryU, setMandatoryU] = useState(false);

  const title = isLogin ? 'Login' : 'Register';
  const messageText = isLogin ? "Don't have an account?" : 'Already have an account?';
  const buttonTitle = isLogin ? 'Login' : 'Register';

  useEffect(() => {
    selectAuth()
      .then((result) => {
        dispatch(actualizarDatos(result.rows._array[0].active));
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const onHandleChangeAuth = () => {
    setIsLogin(!isLogin);
    setMail('');
    setPass('');
    setName('');
    setZoneHour('');
    setError('');
  };

  async function onHandleAuth() {
    setIsLoading(true);
    if (mail === '') {
      setMandatoryM(true);
    } else {
      setMandatoryM(false);
    }
    if (pass === '') {
      setMandatoryP(true);
    } else {
      setMandatoryP(false);
    }
    if (name === '') {
      setMandatoryN(true);
    } else {
      setMandatoryN(false);
    }
    if (zoneHour === '') {
      setMandatoryU(true);
    } else {
      setMandatoryU(false);
    }
    if (
      (isLogin && mail !== '' && pass !== '') ||
      (!isLogin && mail !== '' && pass !== '' && name !== '' && zoneHour !== '')
    ) {
      dispatch(
        isLogin
          ? signIn({ email: mail, password: pass })
          : signUp({ email: mail, password: pass, nombre: name, utc: zoneHour })
      ).then((response) => {
        if (check) {
          deleteAuth();
          if (response.localId !== undefined) {
            authID(response.localId, true)
              .then((result) => {
                dispatch(isLogin ? actualizarDatos(true) : actualizarDatos(false));
              })
              .catch((error) => {
                console.error('Error inserting URL:', error);
              });
          }
        } else {
          deleteAuth();
          if (response.localId !== undefined) {
            authID(response.localId, false)
              .then((result) => {
                dispatch(isLogin ? actualizarDatos(true) : actualizarDatos(false));
              })
              .catch((error) => {
                console.error('Error inserting URL:', error);
              });
          }
        }
        if (response.error) {
          setError(response.error.message);
          setIsLoading(false);
        } else {
          setError('Cuenta Registrada, Inicie Sesion');
          setIsLoading(false);
          if (!isLogin) setIsLogin(!isLogin);
        }
      });
      setMail('');
      setPass('');
      setName('');
      setZoneHour('');
      setError('');
    } else {
      setIsLoading(false);
    }
  }

  const handleInputChange = (text) => {
    const cleanedValue = text.replace(/[^0-9-]/g, '');
    if (cleanedValue === '') {
      setZoneHour('');
    } else {
      const numericValue = parseInt(cleanedValue, 10);
      if (
        (!isNaN(numericValue) && numericValue >= -11 && numericValue <= 12) ||
        cleanedValue === '-'
      ) {
        setZoneHour(cleanedValue);
      }
    }
  };
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles(theme).container}>
        <View style={styles(theme).inputs}>
          <Text style={styles(theme).title}>{title}</Text>
          <Auth_Input
            placeholder="emai@gmail.com"
            label="Email"
            autoCorrect={false}
            hidePassword={false}
            onChangeText={(text) => setMail(text)}
            value={mail}
            mandatory={mandatoryM}
          />
          <Auth_Input
            placeholder="*********"
            label="Contraseña"
            hidePassword
            autoCorrect={false}
            onChangeText={(text) => setPass(text)}
            value={pass}
            mandatory={mandatoryP}
          />
          {!isLogin ? (
            <Auth_Input
              placeholder="Mauricio"
              label="Nombre"
              hidePassword={false}
              autoCorrect={false}
              onChangeText={(text) => setName(text)}
              value={name}
              mandatory={mandatoryN}
            />
          ) : null}
          {!isLogin ? (
            <Auth_Input
              placeholder="-3"
              label="Zona Horaria"
              hidePassword={false}
              autoCorrect={false}
              onChangeText={handleInputChange}
              value={zoneHour}
              mandatory={mandatoryU}
            />
          ) : null}
          <View style={styles(theme).linkContainer}>
            <TouchableOpacity style={styles(theme).link} onPress={onHandleChangeAuth}>
              <Text style={styles(theme).linkText}>{messageText}</Text>
            </TouchableOpacity>
            {isLogin ? (
              <CheckBox
                title="Mantener Sesion Iniciada"
                checked={check}
                onPress={() => setCheck(!check)}
              />
            ) : null}
          </View>
          <View style={styles(theme).submitContainer}>
            <Button title={buttonTitle} color="#212121" onPress={onHandleAuth} />
          </View>
          {isLoading ? <ActivityIndicator size="large" color="#009854" /> : null}
          <View style={styles(theme).errorContainer}>
            <Text style={styles(theme).errorMessage}>{error}</Text>
          </View>
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

export default Acceso;
