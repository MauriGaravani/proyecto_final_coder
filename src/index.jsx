import { useFonts } from 'expo-font';
import { ActivityIndicator, View } from 'react-native';
import { Provider } from 'react-redux';

import { init } from './db';
import AppNavigator from './navigation';
import store from './store/index';
import { styles } from './styles';

init();

export default function App() {
  const [loaded] = useFonts({
    'PTSans-BoldItalic': require('../assets/fonts/PTSans-BoldItalic.ttf'),
    'PTSans-Italic': require('../assets/fonts/PTSans-Italic.ttf'),
    'PTSans-Regular': require('../assets/fonts/PTSans-Regular.ttf'),
    'PTSans-Bold': require('../assets/fonts/PTSans-Bold.ttf'),
  });

  if (!loaded) {
    return (
      <View style={styles.loaderContainer}>
        <ActivityIndicator size="large" color="#212121" />
      </View>
    );
  }
  return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  );
}
