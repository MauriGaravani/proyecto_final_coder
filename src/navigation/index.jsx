import { NavigationContainer } from '@react-navigation/native';
import { useSelector } from 'react-redux';

import AuthNavigator from './auth';
import TabsNavigator from './tabs';

const Navigation = () => {
  const datos = useSelector((state) => state.datos.datos);

  return <NavigationContainer>{datos ? <TabsNavigator /> : <AuthNavigator />}</NavigationContainer>;
};

export default Navigation;
