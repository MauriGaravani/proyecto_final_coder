import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { Acceso } from '../../screens';

const Stack = createNativeStackNavigator();

const AuthNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="Acceso"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Acceso" component={Acceso} />
    </Stack.Navigator>
  );
};

export default AuthNavigator;
