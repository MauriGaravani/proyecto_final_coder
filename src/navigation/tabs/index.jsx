import { MaterialCommunityIcons } from '@expo/vector-icons';
import Ionicons from '@expo/vector-icons/Ionicons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useEffect, useRef } from 'react';
import { Animated } from 'react-native';
import { useSelector } from 'react-redux';

import { getColors } from '../../constants';
import { Camara, Configuracion, Inicio } from '../../screens';

const BottomTab = createBottomTabNavigator();

const TabsNavigator = () => {
  const theme = useSelector((state) => state.color.theme);
  const COLORS = getColors(theme);
  const animatedValue = useRef(new Animated.Value(0)).current;
  const tabIconAnimation = () => {
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
    }).start();
  };

  useEffect(() => {
    tabIconAnimation();
  }, [tabIconAnimation]);
  return (
    <BottomTab.Navigator
      initialRouteName="InicioTab"
      screenOptions={{
        headerShown: false,
        tabBarLabelStyle: {
          fontSize: 12,
        },
        tabBarStyle: {
          backgroundColor: COLORS.primary,
        },
        tabBarActiveTintColor: COLORS.item,
        tabBarInactiveTintColor: 'black',
        tabBarIconStyle: {
          fontSize: 22,
        },
      }}>
      <BottomTab.Screen
        name="InicioTab"
        component={Inicio}
        options={{
          tabBarLabel: 'Inicio',
          tabBarIcon: ({ focused, color, size }) => (
            <Animated.View style={{ opacity: animatedValue }}>
              <Ionicons name={focused ? 'home' : 'home-outline'} size={size} color={color} />
            </Animated.View>
          ),
        }}
      />
      <BottomTab.Screen
        name="CamaraTab"
        component={Camara}
        options={{
          tabBarLabel: 'Camara',
          tabBarIcon: ({ focused, color, size }) => (
            <Ionicons name={focused ? 'camera' : 'camera-outline'} size={size} color={color} />
          ),
        }}
      />
      <BottomTab.Screen
        name="ConfiguracionTab"
        component={Configuracion}
        options={{
          tabBarLabel: 'Configuracion',
          tabBarIcon: ({ focused, color, size }) => (
            <MaterialCommunityIcons
              name={focused ? 'cog' : 'cog-outline'}
              size={size}
              color={color}
            />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
};

export default TabsNavigator;
